﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Reflection;

namespace Teste_reflection
{
    public abstract class AbstractDao<T> 
    {
        protected Type  type;
        public AbstractDao()
        {
          this.type = typeof(T);
    }

        public SqlConnection getConnection()
        {
            SqlConnection conn = new SqlConnection();

            conn.ConnectionString =
           "Data Source=localhost;" +
           "Initial Catalog=Teatru;" +
           "User id=sa;" +
           "Password=mahamaha;";
            conn.Open();
            return conn;

        }
        public List<T> formObjects(SqlDataReader reader)
        {
            List<T> list = new List<T>();

            while(reader.Read())
            {
                T nouveau = (T) Activator.CreateInstance(type);
                foreach(var f in this.type.GetFields())
                {
                    this.type.GetField(f.Name).SetValue(nouveau,reader[f.Name]);
                }
                list.Add(nouveau);
            }
            return list;
        }
        public List<T>  GetAll()
        {
            string tableName = getTableName();

            SqlConnection conn = this.getConnection();
                List<T> all = new List<T>();
            string query = "Select * from " + tableName;
            Console.WriteLine("The query is" + query);
           
            

            SqlCommand cmd = new SqlCommand(query, conn);
            SqlDataReader reader = cmd.ExecuteReader();

            return formObjects(reader);
        }

        public string getTableName()
        {
            return (string)type.GetMethod("getTableName").Invoke(Activator.CreateInstance(type), null);
        }
        public void Delete(string[] fieldString,T obj)
        {
            List <FieldInfo> fields = new List<FieldInfo>();
            foreach (var s in fieldString)
            {
               
                fields.Add(this.type.GetField(s));

            }
            string tableName = getTableName();
            SqlConnection conn = this.getConnection();
            string query = "Delete from " + tableName + " where ";
            string andString = "";
            foreach (var f in fields)
            {
                query += andString;
                 query+= f.Name + "=" + f.GetValue(obj);
                andString = " and ";
            }
            Console.WriteLine(query);
            SqlCommand command = new SqlCommand(query, conn);
            command.ExecuteNonQuery();
        }
        public void DeleteById(int id)
        {
            SqlConnection conn = this.getConnection();
            string tableName = getTableName();

            string query = "Delete from " + tableName + " where id="+id.ToString();
           
            Console.WriteLine(query);
            SqlCommand command = new SqlCommand(query, conn);
            command.ExecuteNonQuery();
        }

        public void Insert(T inserted)
        {
            string tableName = getTableName();

            SqlConnection conn = this.getConnection();
            string query = "Insert into " + tableName + " (";

            foreach (var f in this.type.GetFields())
            {
                query += f.Name + ",";

            }

            query = query.Substring(0, query.Length - 1);//delete last fking comma
            query += ") Values(";
            foreach (var f in this.type.GetFields())
            {
                query +="@"+ f.Name + ",";

            }
            query = query.Substring(0, query.Length - 1);//delete last fking comma again
            query += ")";
            SqlCommand command = new SqlCommand(query, conn);

            //put values in params
            foreach (var f in this.type.GetFields())
            {
                command.Parameters.AddWithValue("@" + f.Name, f.GetValue(inserted));
            }

            Console.WriteLine(query);
            command.ExecuteNonQuery();


            Console.WriteLine(query);





        }
    }
}
