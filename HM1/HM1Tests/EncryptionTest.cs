﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using HM1.Bll;
namespace HM1Tests
{
    [TestClass]
    public class EncryptionTest
    {
        [TestMethod]
        public void testEncryption()
        {
            Core clientCore = new Core();
            string unEncrypted = "passwordTest";
            Assert.AreEqual("899b6ea89c20314d457761ec64186c46", Core.GetMd5Hash(unEncrypted));

        }
    }
}
