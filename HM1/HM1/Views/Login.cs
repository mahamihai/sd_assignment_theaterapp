﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using HM1.Views;
using HM1.Bll.Models;
using HM1.Bll;
namespace HM1
{
    public partial class Form1 : Form
    {
        private readonly Core services;
     
        public Form1()
        {
            InitializeComponent();
           
            this.services = new Core();
        }
      
        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void textBox2_TextChanged(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            string username = textBox1.Text;
            string password = textBox2.Text;
            this.services.checkUser(username, password);
            if(this.services.loginOK())
            {
                string permission = this.services.AccesLevel;
                if(permission.Contains("admin"))
                {
                    //Admin admin = new Admin();
                    //AdminUI app = new AdminUI(admin);
                    //app.Show();
                    Admin admin = new Admin();
                    AdminUI adminUI = new AdminUI();
                    adminUI.Show();
                }
                else
                {

                    CashierService cashier = new CashierService();
                    BaseForm cashierUI = new BaseForm();
                    cashierUI.Show();

                }


            }
            else
            {
                MessageBox.Show("User not recognized");
            }

        }
        void populateBoxes(object obj)
        {
            /*

            Type r = obj.GetType();

            for (int i = 0; i < r.GetFields().Length; i++)
            {
                this.txBoxes.ElementAt(i).Text = r.GetFields()[i].GetValue(obj).ToString();

            }
            */
          
        }



        private void button2_Click(object sender, EventArgs e)
        {

           

 
        }

     
        private void dataGridView1_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            DataGridView aux = (DataGridView)sender;
            DataGridViewRow nouveau = aux.CurrentRow;
            int rowNr = aux.SelectedCells[0].RowIndex;
            int id = Convert.ToInt32(aux.Rows[rowNr].Cells["id"].Value.ToString());
            Dao<UserBLL> dao = new Dao<UserBLL>();
            UserBLL us = dao.getById(id);
            populateBoxes(us);


        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }
    }
}
