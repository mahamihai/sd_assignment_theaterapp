﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using HM1.Bll.Models;
using HM1.Bll;
namespace HM1
{
    public partial class BaseForm : Form
    {
        protected List<TextBox> txBoxes;
        protected Type txBoxType=null;
        private  CashierService services;
        protected int Mode;
        protected int selectedID;
        protected ShowBLL selectedShow=null;
        protected int maxRows = 100;
        protected int maxCollumns = 100;
      
        public BaseForm()
        {
            InitializeComponent();
            this.dataGridView1.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_CellClick);
            this.dataGridView1.AllowUserToAddRows = false;
            this.services = new CashierService();
            disableAll();
        }
        
        public static object GetTfromString<T>(string mystring)
        {
            var foo = TypeDescriptor.GetConverter(typeof(T));
            return (T)(foo.ConvertFromInvariantString(mystring));
        }
        public object objectFromBoxes()
        {
            var obj = Activator.CreateInstance(txBoxType);
            try
            {
                
                for (int i = 0; i < txBoxes.Count; i++)
                {
                    Type fieldType = txBoxType.GetProperties()[i].PropertyType;

                    var foo = Convert.ChangeType(txBoxes[i].Text.Trim(), fieldType);

                    txBoxType.GetProperties()[i].SetValue(obj, foo); //convert data to match field type
                }
            }
            catch

            {
                MessageBox.Show("Invalid input");
                return null;
            }

            return obj;

        }

        protected void button1_Click(object sender, EventArgs e)
        {
            /*
            if (txBoxType != null)
            {
                object obBoxes = objectFromBoxes();
                this.services.update(obBoxes, txBoxType);
                setupTable(txBoxType);
            }
            */

        }

        protected void button3_Click(object sender, EventArgs e)
        {

        }

        protected void BaseForm_Load(object sender, EventArgs e)
        {

        }
        public void setTxBoxes(List<TextBox> txBoxes,Type t)
        {
            this.flowLayoutPanel1.Controls.Clear();
            this.txBoxType = t;
            foreach (var f in txBoxes)
            {
                this.flowLayoutPanel1.Controls.Add(f);

            }
            this.txBoxes = txBoxes;

        }
        void populateBoxes(object obj)
        {

            Type r = obj.GetType();

            for (int i = 0; i < r.GetFields().Length; i++)
            {
                this.txBoxes.ElementAt(i).Text = r.GetFields()[i].GetValue(obj).ToString();

            }

        }
        public void copyToBoxes(int id)
        {
           
        }
        private void dataGridView1_CellClick(object sender, DataGridViewCellEventArgs e)
        {

            DataGridView aux = (DataGridView)sender;
            DataGridViewRow nouveau = aux.CurrentRow;
            int rowNr = aux.SelectedCells[0].RowIndex;
            var selected = Activator.CreateInstance(this.txBoxType);

            int id = Convert.ToInt32(aux.Rows[rowNr].Cells["id"].Value.ToString());
            for (int i = 0; i < txBoxes.Count; i++)
            {
                this.txBoxes[i].Text = aux.Rows[rowNr].Cells[i].Value.ToString();
            } 
            this.selectedID = id;
            
           
            



        }

        

        protected virtual void button2_Click(object sender, EventArgs e)
        {
            /*
            if (txBoxType != null)
            {
                var delete = this.services.GetType().GetMethod("deleteById");
                var deleteRef = delete.MakeGenericMethod(txBoxType);
                deleteRef.Invoke(this.services, new object[] { this.selectedID });
                setupTable(txBoxType);
            }
            */
        }

        protected void button3_Click_1(object sender, EventArgs e)
        {
            /*
            if (txBoxType != null)
            {
                object obBoxes = objectFromBoxes();
                this.services.insert(obBoxes, txBoxType);
                setupTable(txBoxType);
            }
            */
        }

        
    

   
        protected   void disableAll()
        {
           
            this.ticketPanel.Visible = false;
            this.dataGridView1.Visible = false;
            this.flowLayoutPanel1.Visible = false;
            this.ticketsB.Visible = false;
        }
        public void setupTable<T>(List<T> list)
        {
            DataTable tab = createTable<T>(list);
            tab = populateTable<T>(tab, list);
            this.dataGridView1.DataSource = tab;
        }
        public void setupTxBoxes<T>()
        {
            var txBoxes = createTextboxes<T>();
            displayBoxes<T>(txBoxes);
        }
        protected virtual void button4_Click(object sender, EventArgs e)
        {
            disableAll();
            this.ticketsB.Visible = true;
           // this.flowLayoutPanel1.Visible = true;
            this.dataGridView1.Visible = true;
            var shows = this.services.getShows();
            setupTable<ShowBLL>(shows);
            setupTxBoxes<ShowBLL>();
          
            



        }

        public void displayBoxes<T>(List<TextBox> txBoxes)
        {
            this.flowLayoutPanel1.Controls.Clear();
            this.txBoxType = typeof(T);
            foreach (var f in txBoxes)
            {
                this.flowLayoutPanel1.Controls.Add(f);

            }
            this.txBoxes = txBoxes;

        }
        public DataTable populateTable<T>(DataTable dt, List<T> items)
        {
            Type type =typeof(T);
           
            foreach (var f in items)
            {
                DataRow dr = dt.NewRow();
                //populate table
                foreach (var t in type.GetProperties())
                {
                    
                    dr[t.Name] = type.GetProperty(t.Name).GetValue(f);
                }
                dt.Rows.Add(dr);


            }
            return dt;

        }
        public DataTable createTable<T>(List<T> items)
        {
            DataTable tab = new DataTable();
            Type t = typeof(T);
            foreach (var f in t.GetProperties())
            {
                tab.Columns.Add(new DataColumn(f.Name, f.PropertyType));

            }


            return tab;
        }
    

    private void flowLayoutPanel1_Paint(object sender, PaintEventArgs e)
        {

         


        }


        public List<TextBox> createTextboxes<T>()
        {

            List<TextBox> txBoxes = new List<TextBox>();
            for (int i = 0; i < typeof(T).GetProperties().Length; i++)
            {
               
                TextBox aux = new TextBox();
                if(i==0)
                {
                    aux.ReadOnly = true;
                }
                aux.Location = new System.Drawing.Point(100 * i, 400);
                aux.Text = i.ToString();
               
                txBoxes.Add(aux);
                aux.Visible = true;
            }
            return txBoxes;
        }

        public void setCrudVisible(bool flag)
        {
            this.flowLayoutPanel1.Visible = flag;
          
        }
      
        public  void cashierTicketB(object sender, EventArgs e)
        {
            disableAll();

            ShowBLL selected = (ShowBLL)objectFromBoxes();

                this.button4.Visible = true;
            this.ticketsB.Visible = false;

            this.dataGridView1.Visible = true;
            this.services.getTicketsForShow(selected);
           
            this.ticketPanel.Visible = true;
            this.selectedShow = selected;
            var tickets = this.services.getTicketsForShow(selected);
            setupTable<TicketBLL>(tickets);
            setupTxBoxes<TicketBLL>();




        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void button10_Click(object sender, EventArgs e)
        {
            
            int row=-1;
            int collumn=-1;
            dynamic obj = objectFromBoxes();
            try
            {

                row = int.Parse(this.textBox1.Text);
                collumn = int.Parse(this.textBox2.Text);
            }
            catch
            {
                MessageBox.Show("Invalid input");
                return;
            }
           
            if (this.services.insertTicket(obj.ShowId, row, collumn))
            {
                MessageBox.Show("Succes");
            }
            else
            {
                MessageBox.Show("Fail");
            }



        }

        private void button5_Click_1(object sender, EventArgs e)
        {
            dynamic tick = objectFromBoxes();
            this.services.deleteReservation(tick);

        }

        private void button6_Click(object sender, EventArgs e)
        {
            int row = -1;
            int collumn = -1;
            dynamic obj = objectFromBoxes();
            row = int.Parse(this.textBox1.Text);
            collumn = int.Parse(this.textBox2.Text);
            if (this.services.updateTicket(obj, row, collumn))
            {
                MessageBox.Show("Succes");
            }
            else
            {
                MessageBox.Show("Fail");
            }

        }
    }
}
