﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using HM1.Bll.Models;
using HM1.Bll;
namespace HM1.Views
{
    public partial class AdminUI : BaseForm
    {
        private readonly Admin services;
        public AdminUI()
        {
            InitializeComponent();
            this.ticketsB.Click -= new System.EventHandler(this.cashierTicketB);

            this.services = new Admin();
            disableAll();
        }

        private void button7_Click(object sender, EventArgs e)
        {
            if (this.services.ExportToCsv(this.selectedShow.Id))
            {
                MessageBox.Show("Success");
            }
        }
        protected   void disableAll()
        {
            this.panel2.Visible = false;
            this.panel1.Visible = false;
            this.ticketPanel.Visible = false;
            this.dataGridView1.Visible = false;
            this.flowLayoutPanel1.Visible = false;
            this.ticketsB.Visible = false;
        }
        private void button8_Click(object sender, EventArgs e)
        {
            disableAll();
            panel1.Visible = true;
            this.flowLayoutPanel1.Visible = true;
            var cashiers = this.services.getCashiers();
         
           
            // this.flowLayoutPanel1.Visible = true;
            this.dataGridView1.Visible = true;
            
            setupTable<CashierBLL>(cashiers);
            setupTxBoxes<CashierBLL>();


        }

        protected override void button4_Click(object sender, EventArgs e)
        {
            disableAll();
            this.ticketsB.Visible = true;
            // this.flowLayoutPanel1.Visible = true;
            this.dataGridView1.Visible = true;
            this.panel2.Visible = true;
            this.flowLayoutPanel1.Visible = true;
            var shows = this.services.getShows();
            setupTable<ShowBLL>(shows);
            setupTxBoxes<ShowBLL>();


        }

        private void button1_Click_1(object sender, EventArgs e)
        {
            var cashier = objectFromBoxes();
           if( this.services.insertCashier(cashier))
            {
                MessageBox.Show("Success");

            }
           else
            {
                MessageBox.Show("Error inserting client");
            }
        }

        private void button2_Click_1(object sender, EventArgs e)
        {
            var cashier = objectFromBoxes();
            if (cashier != null)
            {
                if (this.services.deleteCashier(cashier))
                {
                    MessageBox.Show("Success");

                }
                else
                {
                    MessageBox.Show("Error deleting client");
                }
            }
        }

        private void button3_Click_2(object sender, EventArgs e)
        {
            var cashier = objectFromBoxes();
            if (cashier != null)
            {
                if (this.services.updateCashier(cashier))
                {
                    MessageBox.Show("Success");

                }
                else
                {
                    MessageBox.Show("Error updating cashier");
                }
            }
        }

        private void button12_Click(object sender, EventArgs e)
        {
           
            var show = objectFromBoxes();
            if (show != null)
            {
                if (this.services.insertShow(show))
                {
                    MessageBox.Show("Success");

                }
                else
                {
                    MessageBox.Show("Error inserting show");
                }
            }

        }

        protected  void ticketButton(object sender, EventArgs e)
        {
           

            ShowBLL selected = (ShowBLL)objectFromBoxes();
            if (selected != null)
            {
                disableAll();
                this.ticketsB.Visible = false;
                this.button4.Visible = true;

                this.dataGridView1.Visible = true;
                this.services.getTicketsForShow(selected);

                this.ticketPanel.Visible = true;
                this.selectedShow = selected;
                var tickets = this.services.getTicketsForShow(selected);
                setupTable<TicketBLL>(tickets);
                setupTxBoxes<TicketBLL>();

            }


        }

        private void button11_Click(object sender, EventArgs e)
        {
            var show = objectFromBoxes();
            if (show != null)
            {
                if (this.services.deleteShow(show))
                {
                    MessageBox.Show("Success");

                }
                else
                {
                    MessageBox.Show("Error deleting show");
                }
            }

        }

        private void button9_Click(object sender, EventArgs e)
        {
            var show = objectFromBoxes();
            if (show != null)
            {
                if (this.services.updateShow(show))
                {
                    MessageBox.Show("Success");

                }
                else
                {
                    MessageBox.Show("Error updating show");
                }
            }

        }
    }
}
