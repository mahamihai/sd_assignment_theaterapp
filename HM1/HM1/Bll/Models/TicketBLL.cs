﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HM1.Bll.Models
{
    public class TicketBLL:ModelBLL
    {


        private int id;


        private int showId;
        private int seatNr;
        private int seatRow;

        public int Id { get => id; set => id = value; }
        public int ShowId { get => showId; set => showId = value; }
        public int SeatNr { get => seatNr; set => seatNr = value; }
        public int SeatRow { get => seatRow; set => seatRow = value; }

        public TicketBLL()
        {

        }

        public TicketBLL(int id, int showId, int seatNr, int seatRow)
        {
            this.Id = id;
            this.ShowId = showId;
            this.SeatNr = seatNr;
            this.SeatRow = seatRow;
        }

      
       
    }
}
