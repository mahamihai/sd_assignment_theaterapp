﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HM1.Bll.Models
{
    public class CashierBLL:ModelBLL
    {
        private int id;
        private string name;
        private string surname;

        public CashierBLL(int id, string name, string surname)
        {
            this.id = id;
            this.name = name;
            this.surname = surname;
        }
        public override void updateTable()
        {
            this.tablename = "dbo.Cashiers";
        }
        public CashierBLL()
        {

        }
        public int Id { get => id; set => id = value; }
        public string Name { get => name; set => name = value; }
        public string Surname { get => surname; set => surname = value; }
    }
}
