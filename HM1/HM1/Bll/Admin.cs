﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using HM1.Dao.Models;
using HM1.Dao;
using HM1.Bll.Models;
namespace HM1.Bll
{
    class Admin : CashierService
    {


        public bool ExportToCsv(int showId)
        {
            try
            {
                List<Ticket> tickets = this.ticketsRepo.getByShowId(showId);
                this.ticketsRepo.WriteToCsv<Ticket>(tickets);
            }
            catch
            {
                return false;
            }

            return true;

        }

        public bool updateShow(object show)
        {
            var changed = this.setupMapper<ShowBLL, Show>().Map<Show>(show);

            return this.showsRepo.UpdateById(changed);
           

            
        }
        public bool insertShow(object show)
        {
            var converted = this.setupMapper<ShowBLL, Show>().Map < Show>(show);
            
             return    this.showsRepo.insertShow(converted);
            
        }
        public List<CashierBLL> getCashiers()
        {
            return this.cashierRepo.getCashiers().Select(this.setupMapper<CashierService, CashierBLL>().Map<CashierBLL>).ToList();
        }
        public bool insertCashier(object cashier)
        {
           var converted=setupMapper<CashierBLL, HM1.Dao.Models.Cashier>().Map<HM1.Dao.Models.Cashier>(cashier);
            bool newUser = this.userRepo.insertUser(new User(-1,"cashier",converted.Name.Trim(), GetMd5Hash(converted.Name.Trim())));
           
           
           return  this.cashierRepo.insertCashier(converted);


        }
        public static string GetMd5Hash(string input)
        {
            MD5 md5Hash = MD5.Create();

            // Convert the input string to a byte array and compute the hash.
            byte[] data = md5Hash.ComputeHash(Encoding.UTF8.GetBytes(input));
            // Create a new Stringbuilder to collect the bytes
            // and create a string.
            StringBuilder sBuilder = new StringBuilder();
            // Loop through each byte of the hashed data 
            // and format each one as a hexadecimal string.
            for (int i = 0; i < data.Length; i++)
            {
                sBuilder.Append(data[i].ToString("x2"));
            }
            // Return the hexadecimal string.
            return sBuilder.ToString();
        }

        
        public bool deleteCashier(object cashier)
        {
            var changed = this.setupMapper<CashierBLL, CashierService>().Map < Cashier>(cashier);
            try
            {
                this.cashierRepo.DeleteById(changed.Id);
                this.userRepo.DeleteByName(changed.Name.Trim());
            }
            catch
            {
                return false;
            }

            return true;
        }

        public bool updateCashier(object cashier)
        {
            var changed = this.setupMapper<CashierBLL, CashierService>().Map<Cashier>(cashier);
            try
            {
                this.cashierRepo.updateCashier(changed);
            }
            catch
            {
                return false;
            }

            return true;

        }

        public bool deleteShow(object show)
        {
            var converted = this.setupMapper<ShowBLL, Show>().Map<Show>(show);
            try
            {
                this.showsRepo.DeleteById(converted.Id);
            }
            catch 
            {
                return false;
            }

            return true;

        }
    }
}
