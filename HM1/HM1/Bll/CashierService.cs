﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections;
using System.Collections.Generic;
using System.Windows.Forms;

using HM1.Dao.Models;
using HM1.Dao;
using HM1.Bll.Models;
using AutoMapper;

namespace HM1.Bll
{

    public  class CashierService
    {
        public int  maxRow=100;
        public int maxCollumn = 100;
        protected CashierRepo cashierRepo;
        protected TicketsRepo ticketsRepo;
        protected UserRepo userRepo;
        protected ShowsRepo showsRepo;
       protected Mapper iMapper;
        public CashierService()
        {
            this.cashierRepo=new CashierRepo();
            this.ticketsRepo=new TicketsRepo();
            this.userRepo=new UserRepo();
            this.showsRepo=new ShowsRepo();
            

        }
        public DataTable createTable<T>(List<T> list)
        {

            Factory<T> fact = new Factory<T>();
            DataTable tab = fact.createTable(list);
            tab = fact.populateTable(tab, list);
            return tab;

        }
        public AutoMapper.IMapper setupMapper<T,U>()
        {
            var config = new MapperConfiguration(cfg => {

                cfg.CreateMap<T, U>();

            });
            var mapper = config.CreateMapper();
            return mapper;
        }
        public List<ShowBLL> getShows()
        {
            ShowsRepo repo=new ShowsRepo();
           

            var t = repo.getShows();
            var config = new MapperConfiguration(cfg => {

                cfg.CreateMap<Show, ShowBLL>();

            });
            var mapper = config.CreateMapper();
            var converted=t.Select(x =>mapper.Map<ShowBLL>(x)).ToList();

            return converted;
        }

        public List<TicketBLL> getTicketsForShow(ShowBLL show)
        {
            if (show == null)
            {
                Console.WriteLine("Null show selected");
                return null;

            }
            else
            {
                
                TicketsRepo tRepo=new TicketsRepo();

                var list= tRepo.getByShowId(show.Id);
                var config = new MapperConfiguration(cfg => {

                    cfg.CreateMap<Ticket, TicketBLL>();

                });
                var mapper = config.CreateMapper();

                return list.Select(x => mapper.Map<TicketBLL>(x)).ToList();
            }
        }

        public bool insertTicket(int showId, int row, int collumn)
        {

            if(row<0 || row>maxRow || collumn<0 || collumn>maxCollumn)
            {
                Console.WriteLine("Not a valid posotion");

                return false;
            }

            
            else
            {
                
                
               
                TicketsRepo tRepo=new TicketsRepo();
                List<Ticket> tickets = tRepo.getTickets();
                List<Ticket> collisions =
                    tickets.Where(x => x.ShowId == showId && x.SeatRow == row && x.SeatNr == collumn).ToList();
                if (collisions.Count() > 0)
                {
                    Console.WriteLine("Alredy present");
                    return false;
                }

                Ticket tic=new Ticket(-1,showId,row,collumn);
               if(  tRepo.insertTicket(tic))
                   return  recomputeNrTickets();
                else
                {
                    return false;
                }

            }
        }

        public bool updateTicket(TicketBLL tick, int row, int collumn)
        {

            if (row < 0 && row > maxRow && collumn < 0 && collumn > maxCollumn)
            {
                Console.WriteLine("Not a valid posotion");

                return false;
            }


            else
            {
                var convert = setupMapper<TicketBLL, Ticket>().Map<Ticket>(tick);
                TicketsRepo tRepo = new TicketsRepo();
                List<Ticket> tickets = tRepo.getTickets();
                
                List<Ticket> collisions =
                    tickets.Where(x => x.ShowId == convert.ShowId && x.SeatRow == row && x.SeatNr == collumn).ToList();
                if (collisions.Count() > 0)
                {
                    Console.WriteLine("Alredy present");
                    return false;
                }

                convert.SeatRow = row;
                convert.SeatNr = collumn;
                if (tRepo.updateTicket(convert))
                    return recomputeNrTickets();
                else
                {
                    return false;
                }
            }
        }
        private bool recomputeNrTickets()
        {
            ShowsRepo showsRepo=new ShowsRepo();
            TicketsRepo ticketsRepo = new TicketsRepo();
            List<Show> shows = showsRepo.getShows();
            foreach (var f in shows)
            {
                int nrTickets = ticketsRepo.getByShowId(f.Id).Count;
                f.Nr_tickets = nrTickets;
                showsRepo.UpdateById(f);
            }

            return true;

        }
        public bool deleteReservation(TicketBLL tick)
        {
            if (tick==null)
            {
                return false;
            }
            else
            {
                TicketsRepo ticketsRepo=new TicketsRepo();
                ticketsRepo.DeleteById(tick.Id);
            }

            return recomputeNrTickets();
        }










    }
}
