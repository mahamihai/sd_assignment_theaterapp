﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using HM1.Dao.Models;
using HM1.Dao;
namespace HM1.Bll
{
    class Factory<T>
    {


        public Factory()
        {

        }

        


       public  DataTable populateTable(DataTable dt, List<T> items)
        {
            foreach (var f in items)
            {
                DataRow dr = dt.NewRow();
                //populate table
                foreach (var t in typeof(T).GetFields())
                {
                    dr[t.Name] = typeof(T).GetField(t.Name).GetValue(f);
                }
                dt.Rows.Add(dr);


            }
            return dt;

        }
        public void WriteToCsv<T>(List<T> list)
            {
            //before your loop
                var csv = new StringBuilder();

            using (var w = new StreamWriter(@"D:\\test.csv"))
            {
                for (int i = 0; i < list.Count; i++)
                {
                    string comma = "";
                    StringBuilder line = new StringBuilder("");
                   foreach(var f in typeof(T).GetFields())
                    {
                        line.Append(comma + f.Name);
                        comma = ",";
                        line.Append(comma + f.GetValue(list[i]));
                        
                     
                    }
                   
                    w.WriteLine(line.ToString());
                    w.Flush();

                }
                w.Flush();
                w.Close();
            }
            
            

        }
        public DataTable createTable(List<T> items)
        {
            DataTable tab = new DataTable();
           foreach (var f in typeof(T).GetFields())
            {
                tab.Columns.Add(new DataColumn(f.Name, f.FieldType));

            }
          

            return tab;
        }
    }
}
