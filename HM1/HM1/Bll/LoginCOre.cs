﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using HM1.Dao;
using HM1.Dao.Models;
namespace HM1.Bll
{
    public class Core
    {
        private  string accesLevel;

        public string AccesLevel => accesLevel;

        public Core()
        {

        }
        public static string GetMd5Hash( string input)
        {
            MD5 md5Hash = MD5.Create();
           
            // Convert the input string to a byte array and compute the hash.
            byte[] data = md5Hash.ComputeHash(Encoding.UTF8.GetBytes(input));
            // Create a new Stringbuilder to collect the bytes
            // and create a string.
            StringBuilder sBuilder = new StringBuilder();
            // Loop through each byte of the hashed data 
            // and format each one as a hexadecimal string.
            for (int i = 0; i < data.Length; i++)
            {
                sBuilder.Append(data[i].ToString("x2"));
            }
            // Return the hexadecimal string.
            return sBuilder.ToString();
        }
       

        public Boolean loginOK()
        {
            return accesLevel != null;
            
        }
      
        public User checkUser(string username, string password)
        {


            if (username.Equals("") || password.Equals(""))
            {
                Console.WriteLine("Error on password");
                return null;
            }
            else
            {
                string encrypted = GetMd5Hash(password);
                UserRepo userRepo = new UserRepo();

                var response=userRepo.checkUser(username, encrypted);
                if (response != null)
                {
                    this.accesLevel = response.accessLevel;
                    return response;

                }
                else
                {
                    return null;
                }
            }

            }


    }
}
