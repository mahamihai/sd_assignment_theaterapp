﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms.VisualStyles;
using HM1.Dao.Models;

namespace HM1.Dao
{
    public class UserRepo : Dao<User>
    {
        public User checkUser(string username, string password)
        {
            List<User> users = this.GetAll();
            var registered = users.Where(x => x.Username.Contains( username) && x.Password.Contains(password)).ToList();
            if (registered.Count() != 0)
            {
                return registered.First();
            }
            else
            {

                return null;
            }
        }

        public bool DeleteByName(string name)
        {
            try
            {
               this.DeleteByField("username",name);
            }
            catch
            {
                return false;

            }

            return true;
        }
        public bool insertUser(User user)
        {
            try
            {
                this.Insert(user);
            }
            catch
            {
                return false;

            }

            return true;

        }
    }
}
