﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using HM1.Dao.Models;

namespace HM1.Dao
{
    public class TicketsRepo:Dao<Ticket>
    {
        public List<Ticket> getByShowId(int id)
        {
            return this.Select(new string[] { "ShowId" }, new string[] {id.ToString()});
        }

        public bool insertTicket(Ticket tick)
        {
            try
            {
                this.Insert(tick);
            }
            catch
            {
                return false;

            }

            return true;

        }

        public bool TIckets2CSV(List<Ticket> list)
        {
            try
            {
                this.WriteToCsv<Ticket>(list);

            }
            catch
            {
                return false;
            }

            return true;

        }
        public void WriteToCsv<T>(List<T> list)
        {
            //before your loop
            var csv = new StringBuilder();

            using (var w = new StreamWriter(@"D:\\test.csv"))
            {
                for (int i = 0; i < list.Count; i++)
                {
                    string comma = "";
                    StringBuilder line = new StringBuilder("");
                    foreach (var f in typeof(T).GetFields())
                    {
                        line.Append(comma + f.Name);
                        comma = ",";
                        line.Append(comma + f.GetValue(list[i]));


                    }

                    w.WriteLine(line.ToString());
                    w.Flush();

                }
                w.Flush();
                w.Close();
            }


        }

        public List<Ticket> getTickets()
        {
            return this.GetAll();
        }

        public bool updateTicket(Ticket ticket)
        {
            try
            {
                this.UpdateBy(new string[] {"Id"}, new string[] {ticket.Id.ToString()}, ticket);
            }
            catch
            {
                return false;
            }

            return true;
        }
        }
}
