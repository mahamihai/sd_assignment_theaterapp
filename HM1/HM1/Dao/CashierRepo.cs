﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HM1.Dao.Models;

namespace HM1.Dao
{
    public class CashierRepo : Dao<Cashier>
    {
        public List<Cashier> getCashiers()
        {
            return this.GetAll();
        }
        public bool insertCashier(Cashier cashier)
        {
            try
            {
                this.Insert(cashier);
            }
            catch
            {
                return false;
            }
            return true;
        }

        public bool updateCashier(Cashier cashier)
        {
            try
            {
                this.UpdateBy(new string[]{"Id"},new string[]{cashier.Id.ToString()}, cashier);
            }
            catch
            {
                return false;
            }
            return true;
        }
    }
}
