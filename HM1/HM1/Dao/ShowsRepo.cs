﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using HM1.Dao.Models;
namespace HM1.Dao
{
   public class ShowsRepo:Dao<Show>
    {
        public List<Show> getShows()
        {
            var shows = this.GetAll();
            return shows;
        }

        public bool UpdateById(Show s)
        {
            try
            {
                this.UpdateBy(new string[] {"Id"}, new string[] {s.Id.ToString()}, s);
            }
            catch 
            {
                return false;
            }

            return true;
        }

        public bool insertShow(Show show)
        {
            try
            {
                this.Insert(show);
            }
            catch
            {
                return false;
            }

            return true;
        }


    }
}
