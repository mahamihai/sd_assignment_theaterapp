﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Reflection;
using HM1.Dao.Models;
namespace HM1
{
    public  class Dao<T> 
    {
        protected Type  type;
        public Dao()
        {
          this.type = typeof(T);
    }

        private SqlConnection getConnection()
        {
            SqlConnection conn = new SqlConnection();

            conn.ConnectionString =
           "Data Source=localhost;" +
           "Initial Catalog=Teatru;" +
           "User id=sa;" +
           "Password=mahamaha;";
            conn.Open();
            return conn;

        }
        private List<T> formObjects(SqlDataReader reader)
        {
            List<T> list = new List<T>();

            while(reader.Read())
            {
                T nouveau = (T) Activator.CreateInstance(type);
                foreach(var f in this.type.GetProperties())
                {
                    this.type.GetProperty(f.Name).SetValue(nouveau,reader[f.Name]);
                }
                list.Add(nouveau);
            }
            return list;
        }
        public Type getType()
        {
            return this.type;
        }
        public T getById(int id)
        {
            string tableName = getTableName();

            SqlConnection conn = this.getConnection();
            string query = "Select * from " + tableName + " where id="+id.ToString();
            

            Console.WriteLine(query);
            SqlCommand cmd = new SqlCommand(query, conn);
            SqlDataReader reader = cmd.ExecuteReader();

            return formObjects(reader)[0];
        }
        protected List<T>  GetAll()
        {
            string tableName = getTableName();

            SqlConnection conn = this.getConnection();
                List<T> all = new List<T>();
            string query = "Select * from " + tableName;
            Console.WriteLine("The query is" + query);
           
            

            SqlCommand cmd = new SqlCommand(query, conn);
            SqlDataReader reader = cmd.ExecuteReader();
        
            return formObjects(reader);
        }

        public string getTableName()
        {
            var t = new Cashier().getTableName();
            return (string)type.GetMethod("getTableName").Invoke(Activator.CreateInstance(type), null);
        }
        public void Delete(string[] fieldString,T obj)
        {
            List <PropertyInfo> fields = new List<PropertyInfo>();
            foreach (var s in fieldString)
            {
               
                fields.Add(this.type.GetProperty(s));

            }
            string tableName = getTableName();
            SqlConnection conn = this.getConnection();
            string query = "Delete from " + tableName + " where ";
            string andString = "";
            foreach (var f in fields)
            {
                query += andString;
                 query+= f.Name + "=" + f.GetValue(obj);
                andString = " and ";
            }
            Console.WriteLine(query);
            SqlCommand command = new SqlCommand(query, conn);
            command.ExecuteNonQuery();
            conn.Close();

        }
        public List<T> Select(string[] fieldString, string[]values)
        {
            List<PropertyInfo> fields = new List<PropertyInfo>();
            foreach (var s in fieldString)
            {

                fields.Add(this.type.GetProperty(s));

            }
            string tableName = getTableName();
            SqlConnection conn = this.getConnection();
            string query = "Select * from " + tableName + " where ";
            string andString = "";
            for (int i = 0; i < fields.Count; i++)
            {
                query += andString;
                query += fieldString[i] + " = " + "\'"+values[i]+"\'";
                andString = " and ";
            }
          
            Console.WriteLine(query);
            SqlCommand cmd = new SqlCommand(query, conn);
            SqlDataReader reader = cmd.ExecuteReader();

            

            return formObjects(reader);
            
        }
        public void UpdateBy(string[] fieldString, string[] values,T updated)
        {
            
            List<PropertyInfo> fields = new List<PropertyInfo>();
            foreach (var s in fieldString)
            {

                fields.Add(this.type.GetProperty(s));

            }
            string tableName = getTableName();
            string query = "Update  " + tableName + " SET ";
            SqlConnection conn = this.getConnection();
            string comma = "";
            foreach(var f in this.type.GetProperties())
            {
                if (!fieldString.Any(f.Name.Contains))
                {
                    query += comma + f.Name + " = " +"\'"+ f.GetValue(updated)+"\'";
                    comma = " , ";
                }
            }
            query += " where ";
            string andString = "";
            for (int i = 0; i < fields.Count; i++)
            {
                query += andString;
                query += fieldString[i] + " = "  + values[i].Split(' ')[0] ;
                andString = " and ";
            }
           
            Console.WriteLine(query);
            SqlCommand cmd = new SqlCommand(query, conn);

            try
            {
                cmd.ExecuteNonQuery();
            }
            catch(Exception ex)
            {
                Console.WriteLine("\nError:" + ex.ToString());
            }


          

        }
        public void DeleteById(int id)
        {
            SqlConnection conn = this.getConnection();
            string tableName = getTableName();

            string query = "Delete from " + tableName + " where id="+id.ToString();
           
            Console.WriteLine(query);
            SqlCommand command = new SqlCommand(query, conn);
            command.ExecuteNonQuery();
            

        }
        public void DeleteByField(string field,string name)
        {
            SqlConnection conn = this.getConnection();
            string tableName = getTableName();

            string query = "Delete from " + tableName + " where "+field+ " like " +" \'"+name+"%\'";

            Console.WriteLine(query);
            SqlCommand command = new SqlCommand(query, conn);
            command.ExecuteNonQuery();


        }

        public void Insert(T inserted)
        {
            string tableName = getTableName();

            SqlConnection conn = this.getConnection();
            string query = "Insert into " + tableName + " (";

            foreach (var f in this.type.GetProperties())
            {
                if (!f.Name.Equals("Id"))
                {
                    query += f.Name + ",";
                }
            }

            query = query.Substring(0, query.Length - 1);//delete last fking comma
            query += ") Values (";
            foreach (var f in this.type.GetProperties())
            {
                if (!f.Name.Equals("Id"))
                    query += "@" + f.Name + ",";
                
            }
            query = query.Substring(0, query.Length - 1);//delete last fking comma again
            query += ")";
            SqlCommand command = new SqlCommand(query, conn);

            //put values in params
            foreach (var f in this.type.GetProperties())
            {
                if(!f.Name.Equals("Id"))
               
                    command.Parameters.AddWithValue("@" + f.Name, f.GetValue(inserted));
                
            }
            query += ";";
            Console.WriteLine(query);
           
                command.ExecuteNonQuery();
            
            Console.WriteLine(query);


         


        }

    }
}
