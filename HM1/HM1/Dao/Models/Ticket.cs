﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HM1.Dao.Models
{
    public class Ticket:Model
    {


        private int id;


        private int showId;
        private int seatNr;
        private int seatRow;

       
        public Ticket()
        {

        }
        public override void updateTable()
        {
            this.tablename = "dbo.Tickets";

        }

        public Ticket(int id, int showId, int seatNr, int seatRow)
        {
            this.Id = id;
            this.ShowId = showId;
            this.SeatNr = seatNr;
            this.SeatRow = seatRow;
        }

        public int Id { get => id; set => id = value; }
        public int ShowId { get => showId; set => showId = value; }
        public int SeatNr { get => seatNr; set => seatNr = value; }
        public int SeatRow { get => seatRow; set => seatRow = value; }
    }
}
