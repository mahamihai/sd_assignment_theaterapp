﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HM1.Dao.Models
{
    public class Cashier:Model
    {
        private int id;
        private string name;
        private string surname;

        public Cashier(int id, string name, string surname)
        {
            this.id = id;
            this.name = name;
            this.surname = surname;
        }
        public override void updateTable()
        {
            this.tablename = "dbo.Cashiers";
        }
        public Cashier()
        {
            
        }

        public int Id { get => id; set => id = value; }
        public string Name { get => name; set => name = value; }
        public string Surname { get => surname; set => surname = value; }
    }
}
