﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HM1.Dao.Models
{
    public class User:Model
    {
        public int id;


        public string accessLevel;
        public string username;
        public string password;

        public string Password { get => password; private set => password = value; }
        public string AccessLevel { get => accessLevel; set => accessLevel = value; }
        public string Username { get => username; set => username = value; }

        public override void updateTable()
        {
            this.tablename = "dbo.Users";
        }
      
        public User(int id, string accessLevel, string username, string password)
        {

            this.id = id;
            this.AccessLevel = accessLevel;
            this.username = username;
            this.password = password;
        }public User()
        {

        }

    }
}
