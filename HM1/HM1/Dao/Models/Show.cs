﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HM1.Dao.Models
{
    public class Show:Model
    {
        private int id;
        private string title;
        private string genre;
        private string distribution;
        private string date;
        private int nr_tickets;

        public int Id { get => id; set => id = value; }
        public string Title { get => title; set => title = value; }
        public string Genre { get => genre; set => genre = value; }
        public string Distribution { get => distribution; set => distribution = value; }
        public string Date { get => date; set => date = value; }
        public int Nr_tickets { get => nr_tickets; set => nr_tickets = value; }

        public Show()
        {

        }
        public override void updateTable()
        {
            this.tablename = "dbo.Shows";
        }

        public Show(int id, string title, string genre, string distribution, string date, int nrTickets)
        {
            this.Id = id;
            this.Title = title;
            this.Genre = genre;
            this.Distribution = distribution;
            this.Date = date;
            this.Nr_tickets = nrTickets;
        }
    }
}
